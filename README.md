#Artise

##Home Page

###Slider Collections
You may have up to 4 collections that are used in a slider on the home page.
These can be set in the custom fields labelled **Home: Slider Collection 1, Home: Slider Collection 2, Home: Slider Collection 3, Home: Slider Collection 4**

The image shown on the slider can also be changed in the field labelled **Slider Image** which can be found when editing the collection of choice.

_Note:
Slider Collection 1 and Slider Collection 2 are shown as the first things on the home page, whilst Slider Collection 3 and Slider Collection 4 are shown further down the page._

###Featured Collections
Under the Slider section, 2 collections can be featured in a carousel. These collections can be shown by selecting the collections
in the custom fields labelled **Home: Featured Collection 1, Home: Featured Collection 2**

_Note: Featured Collection 1 is shown directly underneath the Slider, whilst Featured Collection 2 is shown further down underneath the Banner Collections._


###Banner Collections
Underneath the first Featured Collection section, you may have up to 4 collections to be featured on the home page, shown as 4 collection blocks.
These are set in the custom fields labelled: **Home: Banner Collection 1, Home: Banner Collection 2, Home: Banner Collection 3, Home: Banner Collection 4**

You can change what image is shown in the banner collections by selecting an image in the field **Banner Image** when editing the desired collection

###Display Collections
Further down the page, under the second set of Slider Collections, two collections can be shown in carousels next to each other.
To select these, find the custom fields labelled **Home: Display Collection 1 and Home: Display Collection 2** and choose the collections to be displayed.

##Footer
### Footer: First Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the left of the three menu options.

_Note: Submenus items will not be displayed_

### Footer: Second Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown in the middle of the three menu options.

_Note: Submenus items will not be displayed_

###Footer: Widget
A widget can be placed to be shown on the right-hand side of the footer, for example a map.
This is done by editing the HTML custom field labelled **Footer: Widget**

##Contact Page
###Map Location
In the Contact page, you can have a map of the store displayed underneath the Contact form.
This can be done by going to the custom field labelled **Map Location** in the Design section and inputting the store name and full address in the field, in the format shown below.

**Store Name, Street Name, Location Post Code**

_NOTE: It is important to get these details exactly as they are shown on Google Maps._

##Blog
###Blog page Banner Image
In the Blog Page where all blogs are shown, a custom image can be set to be shown in a banner.
This is set by selecting an image in the custom field labelled **Blog page Banner Image**.


##Collection
###Collection Heading
A collection may have a heading that is visible in the Slider Collections. This is done by typing in a Heading in the field labelled **Collection Heading** when editing the collection of choice.