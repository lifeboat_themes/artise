<nav>
    <ul>
        <% loop $SiteSettings.MainMenu.MenuItems %>
            <% if $Children.count %>
                <li><a href="#">$Title</a>
                    <ul>
                        <% loop $Children %>
                            <li><a href="$Link">$Title</a></li>
                        <% end_loop %>
                    </ul>
                </li>
            <% else %>
                <li <% if isCurrent %>class="active"<% end_if %>>
                    <a href="$Link">$Title</a>
                </li>
            <% end_if %>
        <% end_loop %>
        <li <% if isCurrent %>class="active"<% end_if %>><a href="/cart">Cart</a> </li>
        <li <% if isCurrent %>class="active"<% end_if %>><a href="$WishList.AbsoluteLink">Wishlist</a></li>
        <li <% if isCurrent %>class="active"<% end_if %>><a href="/customer/profile">Profile</a></li>
    </ul>
</nav>