<% if $Theme.CustomField('SliderCollection1').Value() && not $Theme.CustomField('SliderCollection2').Value() %>
    <div class="slider-area">
        <div class="slider_img">
            <% if $Theme.CustomField('SliderCollection1').Value().CustomField('SliderImage').Value() %>
                <img class="img-fluid" src="$Theme.CustomField('SliderCollection1').Value().CustomField('SliderImage').Value().Link" alt="$Theme.CustomField('SliderCollection1').Value().Title" title="#$Theme.CustomField('SliderCollection1').Value().ID"/>
            <% else %>
                <img class="img-fluid" src="$Theme.CustomField('SliderCollection1').Value().Image.Link" alt="$Theme.CustomField('SliderCollection1').Value().Title" title="#$Theme.CustomField('SliderCollection1').Value().ID"/>
            <% end_if %>
        </div>
        <div id="$Theme.CustomField('SliderCollection1').Value().ID" class="nivo-html-caption">
            <div class="slide_all_2">
                <h1 class="wow fadeInLeft" data-wow-delay=".4s" data-wow-duration="1.1s">$Theme.CustomField('SliderCollection1').Value().CustomField('CollectionHeading').Value()</h1>
                <h2 class="wow fadeInRight" data-wow-delay=".8s" data-wow-duration="1.3s">$Theme.CustomField('SliderCollection1').Value().Title</h2>
                <div class="slider-btn  wow bounceInUp" data-wow-delay=".7s" data-wow-duration="1.3s">
                    <a href="$Theme.CustomField('SliderCollection1').Value().AbsoluteLink">view more</a>
                </div>
            </div>
        </div>
    </div>
<% end_if %>
<% if $Theme.CustomField('SliderCollection2').Value() && not $Theme.CustomField('SliderCollection1').Value() %>
    <div class="slider-area">
        <div class="slider_img">
            <% if $Theme.CustomField('SliderCollection2').Value().CustomField('SliderImage').Value() %>
                <img src="$Theme.CustomField('SliderCollection2').Value().CustomField('SliderImage').Value().Link" alt="$Theme.CustomField('SliderCollection2').Value().Title" title="#$Theme.CustomField('SliderCollection2').Value().ID"/>
            <% else %>
                <img src="$Theme.CustomField('SliderCollection2').Value().Image.Link" alt="$Theme.CustomField('SliderCollection2').Value().Title" title="#$Theme.CustomField('SliderCollection2').Value().ID"/>
            <% end_if %>
        </div>
        <div id="$Theme.CustomField('SliderCollection2').Value().ID" class="nivo-html-caption">
            <div class="slide_all_2">
                <h1 class="wow fadeInLeft" data-wow-delay=".4s" data-wow-duration="1.1s">$Theme.CustomField('SliderCollection2').Value().CustomField('CollectionHeading').Value()</h1>
                <h2 class="wow fadeInRight" data-wow-delay=".8s" data-wow-duration="1.3s">$Theme.CustomField('SliderCollection2').Value().Title</h2>
                <div class="slider-btn  wow bounceInUp" data-wow-delay=".7s" data-wow-duration="1.3s">
                    <a href="$Theme.CustomField('SliderCollection2').Value().AbsoluteLink">view more</a>
                </div>
            </div>
        </div>
    </div>
<% end_if %>
<% if $Theme.CustomField('SliderCollection1').Value() && $Theme.CustomField('SliderCollection2').Value() %>
    <div class="slider-area">
        <div class="slider_img">
            <img src="<% if $Theme.CustomField('SliderCollection1').Value().CustomField('SliderImage').Value() %>$Theme.CustomField('SliderCollection1').Value().CustomField('SliderImage').Value().Link <% else %> $Theme.CustomField('SliderCollection1').Value().Image.Link <% end_if %>" alt="$Theme.CustomField('SliderCollection1').Value().Title" title="#$Theme.CustomField('SliderCollection1').Value().ID"/>
            <img src="<% if $Theme.CustomField('SliderCollection2').Value().CustomField('SliderImage').Value() %>$Theme.CustomField('SliderCollection2').Value().CustomField('SliderImage').Value().Link <% else %> $Theme.CustomField('SliderCollection2').Value().Image.Link <% end_if %>" alt="$Theme.CustomField('SliderCollection2').Value().Title" title="#$Theme.CustomField('SliderCollection2').Value().ID"/>
        </div>
        <div id="$Theme.CustomField('SliderCollection1').Value().ID" class="nivo-html-caption">
            <div class="slide_all_2">
                <h1 class="wow fadeInLeft" data-wow-delay=".4s" data-wow-duration="1.1s">$Theme.CustomField('SliderCollection1').Value().CustomField('CollectionHeading').Value()</h1>
                <h2 class="wow fadeInRight" data-wow-delay=".8s" data-wow-duration="1.3s">$Theme.CustomField('SliderCollection1').Value().Title</h2>
                <div class="slider-btn  wow bounceInUp" data-wow-delay=".7s" data-wow-duration="1.3s">
                    <a href="$Theme.CustomField('SliderCollection1').Value().AbsoluteLink">view more</a>
                </div>
            </div>
        </div>
        <div id="$Theme.CustomField('SliderCollection2').Value().ID" class="nivo-html-caption">
            <div class="slide_all_2">
                <h1 class="wow fadeInLeft" data-wow-delay=".4s" data-wow-duration="1.1s">$Theme.CustomField('SliderCollection2').Value().CustomField('CollectionHeading').Value()</h1>
                <h2 class="wow fadeInRight" data-wow-delay=".8s" data-wow-duration="1.3s">$Theme.CustomField('SliderCollection2').Value().Title</h2>
                <div class="slider-btn  wow bounceInUp" data-wow-delay=".7s" data-wow-duration="1.3s">
                    <a href="$Theme.CustomField('SliderCollection2').Value().AbsoluteLink">view more</a>
                </div>
            </div>
        </div>
    </div>
<% end_if %>