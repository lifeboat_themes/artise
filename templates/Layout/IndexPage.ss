<% if $Theme.CustomField('SliderCollection1').Value() || $Theme.CustomField('SliderCollection1').Value() %>
    <% include SliderArea %>
<% end_if %>
<br/>
<% if $Theme.CustomField('FeaturedCollection1').Value() %>
    <% with $Theme.CustomField('FeaturedCollection1').Value() %>
        <div class="new-product-area pt-80 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <h2>$Title</h2>
                            <p>$Heading</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-carousel-active">
                        <% loop $Products.limit(8) %>
                            <div class="col-lg-12">
                                <% include ProductCard Product=$Me %>
                            </div>
                        <% end_loop %>
                    </div>
                </div>
            </div>
        </div>
    <% end_with %>
<% end_if %>
<br/>
<% if $Theme.CustomField('BannerCollection1').Value() || $Theme.CustomField('BannerCollection2').Value() || $Theme.CustomField('BannerCollection3').Value() || $Theme.CustomField('BannerCollection4').Value() %>
    <div class="banner-area">
        <div class="container">
            <div class="row">
                <% include BannerArea %>
            </div>
        </div>
    </div>
<% end_if %>
<br/>
<% if $Theme.CustomField('FeaturedCollection2').Value() %>
    <% with $Theme.CustomField('FeaturedCollection2').Value() %>
        <div class="new-product-area pt-80 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <h2>$Title</h2>
                            <p>$Heading</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-carousel-active">
                        <% loop $Products.limit(8) %>
                            <div class="col-lg-12">
                                <% include ProductCard Product=$Me %>
                            </div>
                        <% end_loop %>
                    </div>
                </div>
            </div>
        </div>
    <% end_with %>
<% end_if %>
<br/>
<% if $Theme.CustomField('SliderCollection3').Value() || $Theme.CustomField('SliderCollection4').Value() %>
    <% include SliderAreaBottom %>
<% end_if %>
<br/>
<% if $Theme.CustomField('DisplayCollection1').Value() || $Theme.CustomField('DisplayCollection2').Value() %>
    <div class="feature-preduct-area pb-50">
        <div class="container">
            <div class="row">
                <% include DisplayCollection %>
            </div>
        </div>
    </div>
<% end_if %>
<br/>
<% if $Blog.Children.count %>
    <div class="blog-area pb-80 dotted-style3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h2>latest blog posts</h2>
                        <p>Check out our latest blog posts below!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog-carousel-active">
                    <% loop $Blog.Children.where('ImageID > 0').sort('Created', 'Desc').limit(4) %>
                        <div class="col-lg-12">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <% if $Image %>
                                        <a href="$AbsoluteLink"><img src="$Image.Fill(345, 222).AbsoluteLink" alt="$Title" /></a>
                                    <% else %>
                                        <a href="$AbsoluteLink"><img src="$SiteSettings.Logo.Fill(345, 222).AbsoluteLink" alt="$Title" /></a>
                                    <% end_if %>
                                </div>
                                <div class="blog-info">
                                    <a href="$AbsoluteLink"><h2>$Title</h2></a>
                                    <% if $MetaDescription %>
                                        <p>$MetaDescription.FirstParagraph.LimitWordCount(30)</p>
                                    <% else %>
                                        <p>$Content.FirstParagraph.LimitWordCount(30)</p>
                                    <% end_if %>
                                    <a href="$AbsoluteLink">Read more <span class="lnr lnr-arrow-right"></span></a>
                                </div>
                            </div>
                        </div>
                    <% end_loop %>
                </div>
            </div>
        </div>
    </div>
<% end_if %>
<br/>
<% if $Integration('Mailchimp') %>
    <div class="contact-area ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mar_b-30">
                    <div class="contuct-info text-center">
                        <h4>Sign up to our newsletter!</h4>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7 offset-lg-1">
                    <div class="alert col mb-2 mx-auto mt-0" id="subscribe-alert" style="display:none;"></div>
                    <div class="search-box">
                        <form action="#" class="mailchimp-subscribe" data-alert="#subscribe-alert">
                            <input type="email" name="email" id="email" placeholder="Enter your email address" required=""/>
                            <button type="submit"><span class="lnr lnr-envelope"> </span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<% end_if %>